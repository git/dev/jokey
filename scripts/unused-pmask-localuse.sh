#!/bin/bash

grep / profiles/package.mask | sed "/^#/d" > /tmp/atoms

echo "checking for unused atom(s) in package.mask..."
unusedpmask=""
for atom in `cat /tmp/atoms`
do
    echo -n "checking '${atom}'... "
    if [[ "`pquery --raw --repo ${PWD} ${atom}`" == "" ]] ; then
        echo "!!! Unused !!!"
        unusedpmask="${unusedpmask} ${atom}"
    else
        echo "has atom(s)"
    fi
done
echo "Done checking package.mask"
echo

cut -d ":" -f 1 profiles/use.local.desc | grep "/" | sort | uniq > /tmp/atoms
unusedlocaldesc=""
echo "checking for unused atom(s) in use.local.desc..."
for atom in `cat /tmp/atoms`
do
    echo -n "checking '${atom}'... "
    if [[ "`pquery --raw --repo ${PWD} ${atom}`" == "" ]] ; then
        echo "!!! Unused !!!"
        unusedlocaldesc="${unusedlocaldesc} ${atom}"
    else
        echo "has atom(s)"
    fi
done
echo "Done checking use.local.desc"
echo

rm /tmp/atoms

if [[ ${unusedpmask} != "" ]] ; then
    echo "The following atoms in package.mask are not in use at the moment:"
    for atom in ${unusedpmask} ; do
        echo ${atom}
    done
fi

if [[ ${unusedlocaldesc} != "" ]] ; then
    echo "The following atoms in use.local.desc are not in use at the moment:"
    for atom in ${unusedlocaldesc} ; do
        echo ${atom}
    done
fi

