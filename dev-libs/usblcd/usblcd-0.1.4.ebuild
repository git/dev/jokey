# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-libs/serdisplib/serdisplib-1.97.5.ebuild,v 1.1 2007/03/11 09:58:51 rbu Exp $

WANT_AUTOCONF="latest"
WANT_AUTOMAKE="latest"
inherit autotools

DESCRIPTION="Library that implements communication with the picoLCD USB LCD device"
HOMEPAGE="http://www.mini-box.com/picoLCD-20x2-OEM?sc=8&category=490"
SRC_URI="http://dev.gentoo.org/~rbu/distfiles/${PN}-src-${PV}.tgz"
# Actually, it's http://resources.mini-box.com/online/picoLCD%2020x2%20(OEM)/Software/Linux/SDKSource/${PN}-src-${PV}.tgz

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND="dev-libs/libusb"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"

	eautoreconf
}

src_install() {
	emake DESTDIR="${D}" install || die "make install failed"
	dodoc AUTHORS README
	use doc && dodoc -r doc
}
