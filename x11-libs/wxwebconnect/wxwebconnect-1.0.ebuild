# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils

DESCRIPTION="XUL interface for HTML rendering in wx Toolkit"
HOMEPAGE="http://www.kirix.com/labs/wxwebconnect"
SRC_URI="http://downloads.kirix.com/labs/wxwebconnect/webconnect-1.0.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S=${WORKDIR}/webconnect

src_prepare() {
	epatch "${FILESDIR}"/${P}-debundle.patch
}

src_compile() {
	cd ${S}/webconnect
	emake -j1|| die "emake failed"
}

src_install() {
	insinto /usr/$(get_libdir)
	doins webconnect/libwebconnect.a
	insinto /usr/include/WebConnect
	doins webconnect/*.h
}