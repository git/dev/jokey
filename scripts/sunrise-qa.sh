#!/bin/bash
# a script to filter and sort pcheck dumps

CHECKDIR=/home/jokey/gentoo/sunrise/sunrise

make_dump() {
	today="$(date +%Y%m%d)-qa-check"
	[[ -d ${today} ]] || mkdir ${today} 
	cd ${today}
	echo "*** Running pcheck on ${CHECKDIR} - will take a while..."
	pcheck -d pkgcore_checks.unstable_only.UnstableOnlyReport -r /usr/portage ${CHECKDIR} &> dump
	cp dump dump-unparsed
	echo "Done!"
	echo
}

filter_keywords() {
	local all_unstable="all unstable"
	local stabled="stabled"
	local no_change="no change"
	local dropped="dropped"
	local vcs_visible="vcs ebuild visible"
	local stupid="mask instead"
	local redundant="keywords are overshadowed"

	echo "*** Filtering keywords-related stuff..."
	for kwd in "${all_unstable}" "${stabled}" "${no_change}" "${dropped}" "${vcs_visible}" "${stupid}" "${redundant}" ; do
		grep "${kwd}" dump > keywords_"${kwd// /_}".log
		echo -e "Filtering "${kwd}"...\t `grep -c "${kwd}" dump` match(es) found."
		sed -i "/${kwd}/d" dump
	done
	echo "Done!"
	echo
}

filter_depends() {
	local nonexistant="nonexistant"
	local unsolvable="unsolvable"
	local malformed="malformed"

	echo "*** Filtering depends-related stuff..."
	for depends in "${nonexistant}" "${unsolvable}" "${malformed}" ; do
		grep "${depends}" dump > depends_"${depends}".log
		echo -e "depends "${depends}"...\t `grep -c "${depends}" dump` problem(s) found."
		sed -i "/${depends}/d" dump
	done

	echo "Doing further sorting..."
	grep masked depends_${unsolvable}.log > depends_${unsolvable}_masked.log
	sed -i "/masked/d" depends_${unsolvable}.log
	grep ppc-macos depends_${unsolvable}.log > depends_${unsolvable}_ppc-macos.log
	sed -i "/ppc-macos/d" depends_${unsolvable}.log
	grep x86-fbsd depends_${unsolvable}.log > depends_${unsolvable}_x86-fbsd.log
	sed -i "/x86-fbsd/d" depends_${unsolvable}.log
	mv depends_${unsolvable}.log depends_${unsolvable}_other.log
	
	echo "Done!"
	echo
}

filter_eclasses() {
	echo -e "*** Filtering deprecated eclasses... \t\t `grep -c "deprecated eclasses" dump` problem(s) found."
	grep "deprecated eclasses" dump > deprecated_eclasses.log
	sed -i "/deprecated eclasses/d" dump
	echo "Done!"
	echo
}

filter_deprecated_flags() {
	echo -e "*** Filtering deprecated flags... \t\t `grep -c "deprecated flags" dump` problem(s) found."
	grep "deprecated flags" dump > deprecated_flags.log
	sed -i "/deprecated flags/d" dump
	echo "Done!"
	echo
}

filter_mirrors() {
	echo -e "*** Filtering unknown mirrors... \t\t `grep -c "no known mirror tier" dump` problem(s) found."
	grep "no known mirror tier" dump > unknown_mirror.log
	sed -i "/no known mirror tier/d" dump
	echo "Done!"
	echo
}


filter_modularx() {
	local unported="not ported"
	local visibility_unported="visibility induced unported"
	local potentially_remove="potentially remove"

	echo "*** Filtering modular X related stuff..."
	for modularx in "${unported}" "${visibility_unported}" "${potentially_remove}" ; do
		grep "${modularx}" dump > modular_x_"${modularx// /_}".log
		echo -e "modular X "${modularx}"...\t `grep -c "${modularx}" dump` problem(s) found."
		sed -i "/${modularx}/d" dump
	done
	echo "Done!"
	echo
}


filter_description() {
	local generic="eclass defined"
	local too_long="over 250 chars"
	local stupid="using the pkg name"
	
	echo "*** Filtering description-related stuff..."
	for desc in "${generic}" "${too_long}" "${stupid}" ; do
		grep "${desc}" dump > description_"${desc// /_}".log
		echo -e "description "${desc}"...\t `grep -c "${desc}" dump` problem(s) found."
		sed -i "/${desc}/d" dump
	done
	echo "Done!"
	echo
}

filter_metadata() {
	local executable="executable"

	echo "*** Filtering metadata-related stuff..."
	for meta in "${executable}" ; do
		grep "${meta}" dump > metadata_"${meta}".log
		echo -e "metadata "${meta}"...\t `grep -c "${meta}" dump` problem(s) found."
		sed -i "/${meta}/d" dump
	done
	echo "Done!"
	echo
}
	

filter_files() {
	local too_big="exceeds 20k"

	echo "*** Filtering files-related stuff..."
	for files in "${too_big}" ; do
		grep "${files}" dump > files_"${files/ /_}".log
		echo -e "filesdir "${files}"...\t `grep -c "${files}" dump` problem(s) found."
		sed -i "/${files}/d" dump
	done
	echo "Done!"
	echo
}

filter_flags() {
	local unstated="unstated flags"
	local unknown="unknown flags"
	local unused="unused flag"
	
	echo "*** Filtering use flags-related stuff..."
	for flags in "${unstated}" "${unknown}" "${unused}" ; do
		grep "${flags}" dump > "${flags/ /_}".log
		echo -e ""${flags}"...\t `grep -c "${flags}" dump` problem(s) found."
		sed -i "/${flags}/d" dump
	done
	echo "Done!"
	echo
}


filter_restrict() {
	local unknown="unknown"
	local deprecated="deprecated"

	echo "*** Filtering restrict-related stuff..."
	for restrict in "${unknown}" "${deprecated}" ; do
		grep "${restrict}" dump > restrict_"${restrict}".log
		echo -e "restrict "${restrict}"...\t `grep -c "${restrict}" dump` problem(s) found."
	done
	for restrict in "${unknown}" "${deprecated}" ; do
		sed -i "/${restrict}/d" dump
	done
	echo "Done!"
	echo
}


filter_vulnerable() {
	echo -e "*** Filtering vulnerable ebuilds... \t\t `grep -c "vulnerable" dump` problem(s) found."
	grep "vulnerable" dump > vulnerable.log
	sed -i "/vulnerable/d" dump
	echo "Done!"
	echo
}

filter_licenses() {
	echo -e "*** Filtering unused licenses... \t\t `grep -c "unused license" dump` match(es) found."
	grep "unused license" dump > unused_licenses.log
	sed -i "/unused license/d" dump
	echo "Done!"
	echo
}


filter_digests() {
	echo -e "*** Filtering digest overlaps... \t\t `grep -c "conflicts with" dump` problem(s) found."
	grep "conflicts with" dump > digest_overlap.log
	sed -i "/conflicts with/d" dump
	echo "Done!"
	echo
}


vdb_hardcoded() {
	echo "*** Checking for hardcoded VDB stuff..."
	local pdir="$(portageq envvar PORTDIR)"
	find "${pdir}" -name '*.eclass' -o -name '*.ebuild' | xargs grep -H var/db/pkg | tee vdb_hardcoded.log >/dev/null 2>&1
}

filter_whitespaces() {
	local trailing="trailing whitespace"
	local leading="leading whitespace"
	local blankline="empty line"
	
	echo "*** Filtering whitespace-related stuff..."
	for whitespaces in "${trailing}" "${leading}" "${blankline}" ; do
		grep "${whitespaces}" dump > "${whitespaces/ /_}".log
		echo -e ""${whitespaces}"...\t `grep -c "${whitespaces}" dump` problem(s) found."
		sed -i "/${whitespaces}/d" dump
	done
	echo "Done!"
	echo
}

filter_remaining() {
	echo "*** Filtering remaining stuff"
	local reversed="is reversed in"
	grep "${reversed}" dump > profiles_reversed.log
	sed -i "/${reversed}/d" dump
	echo "*** The rest is unsorted..."
	echo
	mv dump remaining_unsorted.log
}

cleanup() {
	echo "*** Cleaning up empty logs..."
	for log in `ls *.log` ; do
		[[ -s "${log}" ]] || ( echo "Deleting empty log: ${log} ..." && rm -f "${log}" )
	done
	echo
	echo "*****************"
        echo "*** ALL DONE! ***"
	echo "*****************"
}


make_dump
filter_keywords
filter_depends
filter_eclasses
filter_deprecated_flags
filter_mirrors
filter_modularx
filter_description
filter_metadata
filter_files
filter_flags
filter_restrict
filter_vulnerable
filter_licenses
filter_digests
filter_whitespaces
#vdb_hardcoded
filter_remaining
cleanup
