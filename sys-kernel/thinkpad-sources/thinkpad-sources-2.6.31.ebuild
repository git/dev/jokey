# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: sys-kernel/thinkpad-sources/thinkpad-sources-2.6.28-r2.ebuild 2009 01 19

ETYPE="sources"
K_WANT_GENPATCHES="base extras"
K_GENPATCHES_VER="2"
inherit kernel-2
detect_version
detect_arch

KEYWORDS="~amd64 ~x86"
IUSE=""
HOMEPAGE="http://dev.gentoo.org/~dsd/genpatches http://www.tuxonice.net http://ibm-acpi.sourceforge.net/"

DESCRIPTION="TuxOnIce + Gentoo patches + Latest THINKPAD-Acpi + PCI-E ASPM + Colored Printk + Libata Powermanagement + Bay Fixes + hdaps + phc + ipw2200 inject"

TUXONICE_VERSION="20090911"
TUXONICE_TARGET="2.6.31"
TUXONICE_SRC="current-tuxonice-for-${TUXONICE_TARGET}.patch-${TUXONICE_VERSION}-v1.bz2"
TUXONICE_URI="http://www.tuxonice.net/downloads/all/${TUXONICE_SRC}"

THINKPAD_ACPI_VERSION="0.23-20090920"
THINKPAD_ACPI_TARGET="2.6.31"
THINKPAD_ACPI_SRC="thinkpad-acpi-${THINKPAD_ACPI_VERSION}_v${THINKPAD_ACPI_TARGET}.patch.gz"
THINKPAD_ACPI_URI="mirror://sourceforge/ibm-acpi/${THINKPAD_ACPI_SRC}"


UNIPATCH_LIST=""

MY_PV="${PV}"
UNIPATCH_LIST="${UNIPATCH_LIST}
	${FILESDIR}/${MY_PV}/colored-printk-2.6.30.patch
	${FILESDIR}/${MY_PV}/linux-phc-0.3.2-2.6.31.patch
	${DISTDIR}/${THINKPAD_ACPI_SRC}
	${FILESDIR}/${MY_PV}/thinkpad-acpi-0.23-20090920-fix1.patch.gz
	${DISTDIR}/${TUXONICE_SRC}
"
#	${FILESDIR}/${MY_PV}/02-ipw2200-inject-for-2.6.27.patch
#	${FILESDIR}/${MY_PV}/mac80211.compat08082009.wl_frag+ack_v1.patch


UNIPATCH_STRICTORDER="yes"
SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI} ${TUXONICE_URI} ${THINKPAD_ACPI_URI} ${IEEE80211_URI} ${HRT_URI}"


RDEPEND="${RDEPEND}
		>=sys-apps/tuxonice-userui-0.7.3
		>=sys-power/hibernate-script-1.99-r1"

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
	einfo "AND send a mail to linux-thinkpad mailinglist,"
	einfo "so the patch could be retested and rerated"
	einfo "In files dir is an example config suitable for T60"
        einfo "and hopefully all pci-express driven Thinkpads"
        einfo "but at all you should try for all Thinkpads"
        einfo "to NOT alter the given storage device controller configuration"
	einfo "for powersaving i recommend to append these kernel options:"
	einfo "usbcore.autosuspend=1 pcie_aspm.policy=powersave"
	einfo "additional powertweaks possible (depending on your hardware):"
	einfo "snd_hda_intel.power_save=1"
	einfo "echo 7 > /sys/bus/pci/drivers/iwl3945/0000\:03\:00.0/power_level"
	einfo "echo min_power > /sys/class/scsi_host/host0/link_power_management_policy"
	einfo "echo 5 >/proc/sys/vm/laptop_mode"
	einfo "echo 1500 >/proc/sys/vm/dirty_writeback_centisecs"
	einfo "echo 1 >/sys/devices/system/cpu/sched_mc_power_savings"
}
