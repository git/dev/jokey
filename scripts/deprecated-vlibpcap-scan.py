#!/usr/bin/python
print "Running deprecated-vlibpcap-scan.py..."

def vlibpcap_deprecation_check(depstr):
	if depstr.find("virtual/libpcap") == -1:
		return False
	return True

import os,sys
os.putenv("PORTDIR_OVERLAY", '')
os.putenv("PORTAGE_CALLER", "repoman")
import portage
if __name__ == "__main__":
	ptree = portage.db["/"]["porttree"]
	pdb=ptree.dbapi
	if len(sys.argv) > 1:
		cp_list = sys.argv[1:]
	else:
		cp_list = pdb.cp_all()
	for cp in cp_list:
		cpv = ptree.dep_bestmatch(cp)
		if cpv:
			try:
				d = ' '.join(pdb.aux_get(cpv, ["DEPEND", "RDEPEND", "PDEPEND"]))
			except KeyError:
				print "cpv %s sucks, kthnx" % cpv
				continue
			if vlibpcap_deprecation_check(d):
				print cp
	
print "Done with deprecated-vlibpcap-scan.py."
