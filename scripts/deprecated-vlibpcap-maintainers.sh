#!/bin/bash
PLIST=`cat deprecated-vlibpcap-20061202.txt`

CVSDIR=${HOME}/cvs
# find either a cvs co of gentoo's portage module or the portage directory, and
# echo the result.
eportdir() 
{
    # does fast cache magic. portageq in particular is really slow... this makes
    # subsequent calls to eportdir() pretty much instantaneous, as opposed to
    # taking several seconds.
    if [[ -d ${HOME}/gentoo-x86 ]] ; then
		eval "eportdir() { echo ${HOME}/gentoo-x86 ; }"
		eportdir $*
	elif [[ -d ${CVSDIR}/gentoo-x86 ]] ; then
		eval "eportdir() { echo ${HOME}/cvs/gentoo-x86 ; }"
		eportdir $*
    else
		eval "eportdir() { echo `portageq portdir` ; }"
		eportdir $*
    fi
}

# find a given ebuild. based upon code by agriffis. does some fancy prediction,
# so it will find categories if necessary (eg efind sparc-dev-sources will find
# sys-kernel/sparc-dev-sources, and does partial name matching). prints the
# result in the form category/package .
efind() 
{
    local efinddir cat pkg
    efinddir=$(eportdir)

    case $1 in
	*-*/*)
	    pkg=${1##*/}
	    cat=${1%/*}
	    ;;

	?*)
	    pkg=${1}
	    cat=$(echo1 ${efinddir}/*-*/${pkg}/*.ebuild)
	    [[ -f $cat ]] || cat=$(echo1 ${efinddir}/*-*/${pkg}*/*.ebuild)
	    [[ -f $cat ]] || cat=$(echo1 ${efinddir}/*-*/*${pkg}/*.ebuild)
	    [[ -f $cat ]] || cat=$(echo1 ${efinddir}/*-*/*${pkg}*/*.ebuild)
		[[ -f $cat ]] || return 1
	    pkg=${cat%/*}
	    pkg=${pkg##*/}
	    cat=${cat#${efinddir}/}
	    cat=${cat%%/*}
	    ;;
    esac

    echo ${cat}/${pkg}
}

# guesses who is to blame for a given package. uses metadata.xml if it's
# available. if not, it will try to use cvs log to determine who commited the
# ebuilds (only if a cvs co is available, of course...)
ewho() 
{
    local pc d metadata f

    pc=$(efind $*)
    d=$(eportdir)
    f=0

    if [[ -z "$pc" ]] ; then
		echo "nothing found for $*"
		return 1
    fi

	echo -e "${pc}:"
    metadata="${d}/${pc}/metadata.xml"
    if [[ -f "${metadata}" ]] ; then
		sed -ne 's,^.*<herd>\([^<]*\)</herd>.*,  herd:  \1,p' \
			"${metadata}"
		sed -ne 's,^.*<email>\([^<]*\)@[^<]*</email>.*,  dev:   \1,p' \
			"${metadata}"
		f=1

    elif [[ -d ${d}/${pc}/CVS ]] ; then
		if [[ ${RUNLEVEL} != "work" ]] ; then
			pushd ${d}/${pc} > /dev/null
			for e in *.ebuild ; do
				echo -n "${e}: "
				cvs log ${e} | sed -e '1,/^revision 1\.1$/d' | sed -e '2,$d' \
				-e "s-^.*author: --" -e 's-;.*--'
			done
			popd > /dev/null
			f=1
		else
			echo "Cannot access cvs.gentoo.org!"
		fi
	fi

    if [[ $f -eq 0 ]] ; then
		echo "Nothing found, so blame ciaranm (who blames seemant)"
		return 1
    fi

    return 0
}

for i in `echo ${PLIST}`
	do
	ewho ${i}
done
