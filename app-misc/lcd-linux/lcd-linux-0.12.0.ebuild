# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit linux-mod eutils

DESCRIPTION="LCD-Linux is a Linux software abstraction layer to drive LCD alphanumeric displays. It features complete VT102 console emulation."
HOMEPAGE="http://lcd-linux.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

IUSE=""
DEPEND="sys-kernel/linux-headers"
RDEPEND=""

# linux-mod variables
BUILD_TARGETS="install"
BUILD_PARAMS="DESTROOT=${D} KERNEL_VERSION=${KV_FULL}"
MODULE_NAMES="lcd-linux(misc:${S}:modules) hd44780(misc:${S}:modules)"

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}/${P}-Makefile.patch"
}

src_compile() {
	mkdir -p ${D}/lib/modules/${KV_FULL}/build/include/linux

	linux-mod_src_compile;
}
