# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-dialup/umtsmon/umtsmon-0.9.ebuild,v 1.1 2008/10/05 09:11:19 mrness Exp $

EAPI=2

ECVS_SERVER="umtsmon.cvs.sourceforge.net:/cvsroot/umtsmon"
ECVS_MODULE="umtsmon"

inherit eutils qt3 cvs

DESCRIPTION="Tool to control and monitor wireless mobile network cards (GPRS, EDGE, WCDMA, UMTS, EV-DO, HSDPA)"
HOMEPAGE="http://umtsmon.sourceforge.net/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="x11-libs/qt:3
	dev-libs/libusb"
RDEPEND="${DEPEND}
	net-dialup/ppp
	sys-apps/pcmciautils"

S=${WORKDIR}/${PN}
# for i18n support
# Supported languages and translated documentation
# Be sure all languages are prefixed with a single space!
MY_AVAILABLE_LINGUAS=" de es it nb_NO nl pl pt_PT pt_BR"
IUSE="${IUSE} ${MY_AVAILABLE_LINGUAS// / linguas_}"

src_compile() {
	PATH=/usr/qt/3/bin:$PATH
	eqmake3 || die "eqmake3 failed"
	emake || die "emake failed"

}

src_install() {
	dobin umtsmon || die "dobin failed"

	domenu umtsmon.desktop
	doicon images/128

	dodoc AUTHORS README TODO
}
